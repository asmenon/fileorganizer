from django.shortcuts import render, redirect, HttpResponse
import re

# Create your views here.


def resolve_choice(request, serial):
    url = request.build_absolute_uri()
    new_url = re.sub(r"resolve/.*", "altfile", url)
    new_url = new_url + "?filter_clause=" + serial
    return redirect(new_url)
