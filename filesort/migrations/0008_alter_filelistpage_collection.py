# Generated by Django 3.2.4 on 2021-06-29 20:51

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('filesort', '0007_alter_filelistpage_collection'),
    ]

    operations = [
        migrations.AlterField(
            model_name='filelistpage',
            name='collection',
            field=models.CharField(choices=[(1, 'Root'), (3, 'Logs'), (2, 'Scripts')], default='Root', max_length=255),
        ),
    ]
