# Generated by Django 3.2.4 on 2021-06-29 18:12

from django.db import migrations
import wagtail.core.blocks
import wagtail.core.fields


class Migration(migrations.Migration):

    dependencies = [
        ('filesort', '0003_filelistpage_collection'),
    ]

    operations = [
        migrations.AddField(
            model_name='filelistpage',
            name='docs',
            field=wagtail.core.fields.StreamField([('heading', wagtail.core.blocks.CharBlock(form_classname='full title'))], null=True),
        ),
    ]
