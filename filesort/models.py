import os
from django.db import models
from wagtail.core.models import Page, Collection
from wagtail.core.fields import RichTextField
from wagtail.admin.edit_handlers import FieldPanel
from wagtail.core import blocks
from wagtail.documents.models import Document
from .const import FILTER_CLAUSE_DICT, COLLECTION_CHOICES

# Create your models here.


class FileExtensionsPage(Page):
    intro = RichTextField(blank=True)
    content_panels = Page.content_panels + [FieldPanel("intro", classname="full")]


class FileListPage(Page):
    # Old implementation. Requires files to be tagged with the collection correctly.
    collection = models.IntegerField(choices=COLLECTION_CHOICES, null=True, blank=True)

    content_panels = Page.content_panels + [FieldPanel("collection")]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request)
        COLLECTION_CHOICES = ((c.id, c.name) for c in Collection.objects.all())
        selected_choice = dict(COLLECTION_CHOICES).get(self.collection)
        document_list = list(Document.objects.all())
        # ~~TODO~~: Filter document_list based on selected_choice.
        for document in list(document_list):
            if str(document.collection) != selected_choice:
                document_list.remove(document)

        context["selected_choice"] = selected_choice
        context["docs"] = document_list
        return context


class AltFilePage(Page):
    intro = RichTextField(blank=True)
    content_panels = Page.content_panels + [FieldPanel("intro", classname="full")]

    def get_context(self, request, *args, **kwargs):
        context = super().get_context(request)
        context["collections"] = COLLECTION_CHOICES
        try:
            filter_clause = request.GET.get("filter_clause")
            file_exts = FILTER_CLAUSE_DICT[filter_clause.lower()]
            all_files = list(Document.objects.all())

            for element in list(all_files):
                filename, file_extension = os.path.splitext(str(element))
                if file_extension not in file_exts:
                    all_files.remove(element)
            context["docs"] = all_files
            context["selected_choice"] = filter_clause
        except:
            # Catch error Failed to find filter_clause
            return context
        return context
