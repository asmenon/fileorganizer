# Use this file to add any extensions to the following tuples.
# To filter by an extension:
# add one of the keys in FILTER_CLAUSE_DICT in the wagtail admin collections interface
from wagtail.core.models import Collection

LOG_FILE_EXTENSIONS = (".txt", ".log")
SCRIPT_FILE_EXTENSIONS = (
    ".py",
    ".apk",
    ".cgi",
    ".pl",
    ".jar",
    ".wsf",
    ".bat",
    ".js",
    ".class",
    ".h",
    ".cs",
    ".vb",
    ".c",
)
CONFIG_FILE_EXTENSIONS = (".cfg", ".ini", "")
MISC_DOC_EXTENSIONS = (
    ".doc",
    ".odt",
    ".pdf",
    ".rtf",
    ".txt",
    ".wpd",
    ".docx",
    ".xls",
    ".xlsx",
    ".csv",
)

COLLECTION_CHOICES = list(((c.id, c.name) for c in Collection.objects.all()))
print(COLLECTION_CHOICES)
FILTER_CLAUSE_DICT = {
    "logs": LOG_FILE_EXTENSIONS,
    "log": LOG_FILE_EXTENSIONS,
    "scripts": SCRIPT_FILE_EXTENSIONS,
    "script": SCRIPT_FILE_EXTENSIONS,
    "misc": MISC_DOC_EXTENSIONS,
    "docs": MISC_DOC_EXTENSIONS,
    "doc": MISC_DOC_EXTENSIONS,
    "config": CONFIG_FILE_EXTENSIONS,
}
