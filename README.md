### What is this repository for?

- A wagtail site to organize documents based on their file extension.

### How do I get set up?

- Setup process - Configure superuser and collections.
- Configuration - Upload documents on the wagtail admin interface and assign them the appropriate collection.
- Dependencies - run 'pip install -r requirements.txt' to resolve any dependencies.
